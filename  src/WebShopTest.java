package OOPEinzelaufaufgabe;



public class WebShopTest {

	public WebShop shop;

	public void setUp() throws Exception {
		shop = new WebShop();
		shop.addItem("Seife", 100);
		shop.addItem("Schokolade", 200);
		shop.addItem("Milch", 130);
		shop.addItem("Butter", 199);
		shop.addItem("Wasser", 50);
		
		shop.addCustomer("Peter");
		shop.addCustomer("Paul");
	}

	
	public void printItem() {
		shop.printItem(3);
	}
	
	public void shoppingTest() {
		
		shop.addItemToCart("Peter", 4);
		shop.addItemToCart("Peter", 4);
		shop.addItemToCart("Peter", 3);
		

		
		Kasse kasse = new Kasse();
		
		assert kasse.countItems(shop,"Peter") == 3;
		assert kasse.countItems(shop,"Peter",4) == 2;
		assert kasse.countItems(shop,"Paul") == 0;
		
		assert kasse.cartSum(shop,"Peter") == 299;
		
		shop.printCustomer();
	}
	
	public void testURLs() {
		shop.addURL("www.shoppingpage.gs");
		shop.addURL("www.webshoppingpage.gs");
		shop.addURL("www.OnlineShoppingpage.gs");
		shop.removeURL("www.webshoppingpage.gs");
		
		assert shop.urls.get(0).equals("www.shoppingpage.gs");
		assert shop.urls.get(1).equals("www.OnlineShoppingpage.gs");
		
	}
	
	

}
